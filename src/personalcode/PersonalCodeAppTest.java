package personalcode;

import static org.junit.Assert.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class PersonalCodeAppTest {

	@Before
	public void setUp() throws Exception {
	}

	@After
	public void tearDown() throws Exception {
	}

	@Test
	public void testValidatePersonalCode() {
		boolean result = PersonalCodeApp.validatePersonalCode("38104242729");
		assertTrue(result);
	}

	@Test
	public void testValidatePersonalCode2() {
		boolean result = PersonalCodeApp.validatePersonalCode(null);
		assertFalse(result);
		
		result = PersonalCodeApp.validatePersonalCode("381");
		assertFalse(result);
	}

	@Test
	public void testValidatePersonalCode3() {
		boolean result = PersonalCodeApp.validatePersonalCode("38104242723");
		assertFalse(result);
	}
	
	@Test
	public void testValidatePersonalCode4() {
		boolean result = PersonalCodeApp.validatePersonalCode("38115202721");
		assertFalse(result);
	}
	
	@Test
	public void testValidatePersonalCode5() {
		boolean result = PersonalCodeApp.validatePersonalCode("38104342724");
		assertFalse(result);
	}
	
	@Test
	public void testValidatePersonalCodeDate() {
		boolean result = PersonalCodeApp.valdateBirthDate("38104242729");
		assertTrue(result);
		
		result = PersonalCodeApp.valdateBirthDate("381RR242729");
		assertTrue(!result);
	}
	
	@Test
	public void testValidatePersonalCodeDate2() {
		boolean result = PersonalCodeApp.valdateBirthDate("38124472729");
		assertFalse(result);
	}
	
}
