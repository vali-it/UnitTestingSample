package personalcode;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class PersonalCodeApp {

	private static final int WEIGHTS[] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 1 };
	private static final int[] WEIGHTS2 = { 3, 4, 5, 6, 7, 8, 9, 1, 2, 3 };

	public static void main(String[] args) {
		System.out.println(validatePersonalCode("38104242729"));
	}

	protected static boolean validatePersonalCode(String personalCode) {
		if (personalCode == null || personalCode.length() != 11) {
			return false;
		}

		boolean result = valdateBirthDate(personalCode) && validateCheckNumber(personalCode);

		return result;
	}

	protected static boolean valdateBirthDate(String personalCode) {
		try {
			int birthYear = getBirthYear(personalCode);
			int birthMonth = Integer.parseInt(personalCode.substring(3, 5));
			int birthDay = Integer.parseInt(personalCode.substring(5, 7));

			SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd");
			String dateString = birthYear + "-" + birthMonth + "-" + birthDay;
			Date birthDate = null;

			birthDate = dateFormat.parse(dateString);

			Calendar calendar = new GregorianCalendar();
			calendar.setTime(birthDate);

			return calendar.get(Calendar.YEAR) == birthYear && calendar.get(Calendar.MONTH) + 1 == birthMonth
					&& calendar.get(Calendar.DAY_OF_MONTH) == birthDay && birthDate.before(new Date());
		} catch (ParseException e) {
			return false;
		} catch (NumberFormatException e) {
			return false;
		}
	}

	private static boolean validateCheckNumber(String personalCode) {
		int[] personalCodeDigits = new int[10];
		for (int i = 0; i < 10; i++) {
			personalCodeDigits[i] = Integer.parseInt(personalCode.substring(i, i + 1));
		}
		int lastDigit = Integer.parseInt(personalCode.substring(10, 11));

		int reminder = getReminder(personalCodeDigits, WEIGHTS);

		if (reminder == 10) {
			reminder = getReminder(personalCodeDigits, WEIGHTS2);

			if (reminder != 10) {
				return reminder == lastDigit;
			} else {
				return lastDigit == 0;
			}
		} else {
			return reminder == lastDigit;
		}
	}

	private static int getReminder(int[] personalCodeDigits, int[] weights) {
		int sum = 0;
		for (int i = 0; i < 10; i++) {
			sum += weights[i] * personalCodeDigits[i];
		}
		return sum % 11;
	}

	protected static int getBirthYear(String personalCode) {
		if (personalCode == null || personalCode.length() < 3) {
			return -1;
		}

		int centuryDeterminant = Integer.parseInt(personalCode.substring(0, 1));
		int birthDateBase = 0;

		switch (centuryDeterminant) {
		case 1:
		case 2:
			birthDateBase = 1800;
			break;
		case 3:
		case 4:
			birthDateBase = 1900;
			break;
		case 5:
		case 6:
			birthDateBase = 2000;
			break;
		default:
			return -1;
		}

		int birthYearInCentury = Integer.parseInt(personalCode.substring(1, 3));

		return birthDateBase + birthYearInCentury;
	}

}
